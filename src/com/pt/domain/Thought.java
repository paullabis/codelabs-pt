package com.pt.domain;

import java.util.Date;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Load;

@Entity
public class Thought {
	@Id Long id;
	private Date createdOn;
	private Text thought;
	private Text thumbnail;
	private int thumbsUp;
	private int thumbsDown;
	
	@Load private Ref<Person> owner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Text getThought() {
		return thought;
	}

	public void setThought(Text thought) {
		this.thought = thought;
	}

	public Text getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Text thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public void incrementThumbsUp() {
		thumbsUp++;
	}
	
	public int getThumbsUp() {
		return thumbsUp;
	}

	public void setThumbsUp(int thumbsUp) {
		this.thumbsUp = thumbsUp;
	}

	public int getThumbsDown() {
		return thumbsDown;
	}

	public void incrementThumbsDown() {
		thumbsDown++;
	}
	
	public void setThumbsDown(int thumbsDown) {
		this.thumbsDown = thumbsDown;
	}

	public Person getOwner() {
		if (owner == null) {
			return null;
		}
		return owner.get();
	}

	public void setOwner(Person owner) {
		this.owner = Ref.create(owner);
	}
	
}
