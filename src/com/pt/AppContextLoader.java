package com.pt;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

import com.googlecode.objectify.ObjectifyService;
import com.pt.domain.Person;
import com.pt.domain.Thought;
public class AppContextLoader extends ContextLoaderListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		ObjectifyService.register(Thought.class);
		ObjectifyService.register(Person.class);
	}
	
}
