package com.pt.service;

import java.util.List;

import com.pt.domain.Person;
import com.pt.domain.Thought;

public interface DataService {
	
	void createPerson(Person person);
	void removePerson(Long id);
	void updatePerson(Person person);
	Person getPerson(Long id);
	List<Person> getAllPerson();

	void createThought(Thought thought);
	void removeThought(Long id);
	void updateThought(Thought thought);
	Thought getThought(Long id);
	List<Thought> getAllThought();
	
	
}
