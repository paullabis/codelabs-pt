package com.pt.service.impl;

import java.util.List;

import com.pt.domain.Person;
import com.pt.domain.Thought;
import com.pt.service.DataService;
import static com.googlecode.objectify.ObjectifyService.ofy;

public class DataServiceImpl implements DataService {

	public void createPerson(Person person) {
		ofy().save().entity(person).now();
	}

	public void removePerson(Long id) {
		ofy().delete().type(Person.class).id(id).now();
	}

	public void updatePerson(Person person) {
		ofy().save().entity(person).now();
	}

	public Person getPerson(Long id) {
		return ofy().load().type(Person.class).id(id).get();
	}

	public List<Person> getAllPerson() {
		return ofy().load().type(Person.class).list();
	}

	public void createThought(Thought thought) {
		ofy().save().entity(thought).now();
	}

	public void removeThought(Long id) {
		ofy().delete().type(Thought.class).id(id).now();
	}

	public void updateThought(Thought thought) {
		ofy().save().entity(thought).now();
	}

	public Thought getThought(Long id) {
		return ofy().load().type(Thought.class).id(id).get();
	}

	public List<Thought> getAllThought() {
		return ofy().load().type(Thought.class).list();
	}
}
