<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!-- Bootstrap -->
	    <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
	    <link href="/css/bootstrap-fileupload.css" rel="stylesheet" media="screen">
	    
	    <style type="text/css">
	    	.input-thought {
	    		width: 500px;
	    	}
	    </style>
	</head>
	<body>
	<div class="container">
			<div class="head">
				<div class="row-fluid">
					<div class="span12">
						<div class="span6">
							<h1 class="muted">Public Thoughts</h1>
						</div>
					</div>
				</div>
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container-fluid">
							<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>
							<a class="brand" href="#" name="top">Navigate</a>
							<div class="nav-collapse collapse">
								<ul class="nav">
									<li><a href="/thought/home"><i class="icon-home"></i> Home</a></li>
									<li><a href="/thought/write"><i class="icon-comment"></i> Write</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		<br/>

		<form class="well span8" action="/thought/update" method="post" enctype="multipart/form-data">
			<input name="id" id="id" type="hidden" value="<c:out value='${thought.id}' />" />
			<c:if test="${not empty errorMessage}">
				<div class="alert alert-error">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <strong>Failure!</strong> <c:out value="${errorMessage}"></c:out>
				</div>
			</c:if>
			<div class="row">
				<div class="span3">
					<label>Image</label>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
							<img src="<c:out value='${thought.thumbnail.value}' />" />
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
						<div>
							<span class="btn btn-file"><span class="fileupload-new">Select image</span>
							<span class="fileupload-exists">Change</span>
							<input id="image" name="image" type="file" /></span> 
							<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
						</div>
					</div>
				</div>
				<div class="span5">
					<label>Name</label> 
					<input name="name" id="name" type="text" class="span3" placeholder="Your Name" value="<c:out value='${thought.owner.name}'/>"> 
					<label>Thought</label>
					<textarea name="thought" id="thought" class="input-xlarge span5" rows="10" placeholder="Enter Your Thoughts"><c:out value='${thought.thought.value}'/></textarea>
				</div>

				<button type="submit" class="btn btn-success pull-right">Tell the world</button>
			</div>
		</form>
	</div>
	
	<script src="/js/jquery-1.9.1.js"></script>
   	<script src="/js/bootstrap.min.js"></script>		
   	<script src="/js/bootstrap-fileupload.min.js"></script>								
	</body>
</html>