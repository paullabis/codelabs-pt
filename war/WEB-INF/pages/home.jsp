<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <!-- Bootstrap -->
	    <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
	    
	    <style type="text/css">
	    	.input-thought {
	    		width: 500px;
	    	}
	    </style>
	</head>
	<body>
	<div class="container">
			<div class="head">
				<div class="row-fluid">
					<div class="span12">
						<div class="span6">
							<h1 class="muted">Public Thoughts</h1>
						</div>
					</div>
				</div>
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container-fluid">
							<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>
							<a class="brand" href="/" name="top">Navigate</a>
							<div class="nav-collapse collapse">
								<ul class="nav">
									<li class="active"><a href="/"><i class="icon-home"></i> Home</a></li>
									<li><a href="/thought/write"><i class="icon-comment"></i> Write</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		
		<br/>
		<c:if test="${not empty errorMessage}">
			<div class="alert alert-error">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Failure!</strong> <c:out value="${errorMessage}"></c:out>
			</div>
			<br/>
		</c:if>
		
		
		<c:if test="${not empty thoughs}">
		<c:forEach items="${thoughs}" var="thought">
		<div class="row">
			<div class="span2">
				<!-- <div class="" style="text-align: center; vertical-align: middle;"> -->
				<div class="pagination-centered">
				 	<a href="#" class="thumbnail" style="margin-left:5px;">
					<c:choose>
						<c:when test="${not empty thought.thumbnail}">
							<img src="${thought.thumbnail.value}" alt="">	
						</c:when>
						<c:otherwise>
							<img src="http://www.placehold.it/260x180/EFEFEF/AAAAAA&text=no+image" alt="">
						</c:otherwise>
					</c:choose>
					</a>
				</div>
			</div>
			<div class="span10">
				<blockquote>
					<p>
						<c:out value="${thought.thought.value}"></c:out> 
					</p>
				</blockquote>
				<p>
					<c:if test="${not empty thought.owner}">
						<i class="icon-user"></i> by <c:out value="${thought.owner.name}"></c:out>
			          | 
					</c:if>
			          	<i class="icon-calendar"></i> <c:out value="${thought.createdOn}"></c:out>
			          | <i class="icon-thumbs-up"></i> <span class="label label-success"><c:out value="${thought.thumbsUp}"></c:out></span> Up
			          | <i class="icon-thumbs-down"></i> <span class="label label-important"><c:out value="${thought.thumbsDown}"></c:out></span> Down
        		</p>
				<div class="btn-group pull-right">
					<a href="/thought/thumbs/up/<c:out value='${thought.id}'/>" class="btn"><i class="icon-thumbs-up"></i> <strong>Thumbs Up</strong></a>
					<a href="/thought/thumbs/down/<c:out value='${thought.id}'/>" class="btn"><i class="icon-thumbs-down"></i> <strong>Thumbs Down</strong></a>
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-th-list"></i> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="/thought/update?id=<c:out value='${thought.id}'/>"><i class="icon-list-alt"></i> Update</a></li>
						<li class="divider"></li>
						<li><a href="/thought/delete?id=<c:out value='${thought.id}'/>"><i class="icon-trash"></i> Remove</a></li>
					</ul>
				</div>
			</div>
		</div>
		<br/>
		</c:forEach>
		</c:if>
	</div>
	<script src="/js/jquery-1.9.1.js"></script>
   	<script src="/js/bootstrap.min.js"></script>					
	</body>
</html>